[rancher-rke2-common-%(channel)s]
name=Rancher RKE2 Common (%(channel)s)
baseurl=https://%(server)s/rke2/%(channel)s/common/centos/8/noarch
enabled=1
gpgcheck=1
gpgkey=https://%(server)s/public.key

[rancher-rke2-%(version)s-%(channel)s]
name=Rancher RKE2 %(version)s (%(channel)s)
baseurl=https://%(server)s/rke2/%(channel)s/%(version)s/centos/8/$basearch
enabled=1
gpgcheck=1
gpgkey=https://%(server)s/public.key
