// https://github.com/coreos/ignition/blob/master/config/v3_2/schema/ignition.json
function(rke2_channel='latest', rke2_version='latest', rke2_server='rpm.rancher.io', profiles, admin_user, admin_pass, admin_key) {
  local all_profiles = {
    qemu: {
      packages: ['qemu-agent'],
    },
    rancher: {
      packages: ['rke2-server'],
      ign: {
        storage: {
          files: [
            { path: '/etc/profile.d/rancher-rke2.sh', mode: 420, contents: 'export PATH=$PATH:/var/lib/rancher/rke2/bin' },
            {
              path: '/etc/yum.repos.d/rancher-rke2.repo',
              mode: 420,
              contents: std.format(
                importstr 'files/rancher-rke2.repo.tpl',
                { server: rke2_server, channel: rke2_channel, version: rke2_version }
              ),
            },
          ],
        },
      },
    },
  },

/*
----------------------- TEMPLATE STARTS HERE ------------------------
*/
  ignition: {
    version: '3.2.0',
  },
  config: {},
  proxy: {},
  security: {},
  storage: {} + all_profiles.rancher.ign.storage,
  systemd: {
    units: [
      { name: 'docker', mask: true },
      {
        name: 'ostree-bootstrap',
        contents: importstr 'systemd/ostree-bootstrap.service',
        dropins: [
          {
            name: 'packages',
            contents: |||
              [Service]
              Environment=OSTREE_PACKAGES="%s"
            ||| % std.join(
              ' ',
              [all_profiles.rancher.packages + all_profiles[profile].packages for profile in profiles][0]
            ),
          },
        ],
      },
    ],
  },
  passwd: {
    users: [
      { name: admin_user, groups: ['docker', 'sudo'], sshAuthorizedKeys: [ admin_key ], passwordHash: admin_pass, system: true },

    ],
  },

}
