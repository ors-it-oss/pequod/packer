variables {
  accelerator = "kvm"
  headless = true
  cpus = 2
  memory = 2048
  disk_size = 10240
  prov_user = "orquestador"
  http_port_min = 5900
  http_port_max = 6400
  vnc_port_min = 5900
  vnc_port_max = 6400
}

variable "use_backing_file" {
  type = bool
}
variable "disk_image" {
  type = bool
}
variable "iso_checksum" {
  type = string
}
variable "iso_url" {
  type = string
}
variable "prov_pass" {
  type = string
}
variable "prov_key" {
  type = string
}
variable "os_version" {
  type = string
}
variable "os_name" {
  type = string
}
variable "build_root" {
  type = string
}
