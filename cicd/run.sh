#!/bin/bash
set -x
incldir=$(dirname $(readlink -f $0))
source $incldir/lib.sh
[[ "$DEBUG" =~ shell || "$DEBUG" =~ all ]] && set -x

####---------------- VARIABLES ----------------####
set -a
CHECKPOINT_DISABLE=1
[[ "$DEBUG" =~ packer || "$DEBUG" =~ all ]] && PACKER_LOG=1

if [[ "$CI" != "true" ]]; then
	CI_PROJECT_DIR=$PWD
	if [[ -d .git && -x /usr/bin/git ]]; then
		CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
	fi
	PACKER_IMG=registry.gitlab.com/ors-it-oss/pequod/containers/brantpoint

	PKR_VAR_prov_user=orquestador
	PKR_VAR_prov_pass=rlystoopid
	PKR_VAR_prov_key=evenstoopider

	PKR_VAR_headless=true

	COREOS_STREAM=testing
	RKE2_CHANNEL=latest
	RKE2_VERSION=latest
	RKE2_SERVER=rpm.rancher.io
fi

PACKER_CACHE_DIR="$CI_PROJECT_DIR/build/cache"
PACKER_TMP_DIR="$CI_PROJECT_DIR/build/tmp"
set +a


####---------------- FUNCTIONS ----------------####
coreos(){
	coreos_prep iso
	PKR_ARGS="$PKR_ARGS --var-file common/iso.pkrvars.hcl"
	packer_run "$@"
}

coreos_qcow(){
	coreos_prep qcow
	PKR_ARGS="$PKR_ARGS --var-file common/qcow.pkrvars.hcl --var iso_url=build/cache/fedora-coreos-33.20210314.2.0-qemu.x86_64.qcow2 --var iso_checksum=0a0e765962d77534bbf351a3cea535c9152e8be3d0e8810fdb50679f9a5b2c51"
	packer_run "$@"

}

ws(){
	# Interactive container session
	container "$@"
}


####---------------- CONTAINER WRAPS ----------------####
cnt_coreos(){
	container cicd/run.sh coreos "$@"
}


####---------------- execute args if not sourced ----------------####
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	cmd="$1"; shift
	$cmd "$@"
fi
